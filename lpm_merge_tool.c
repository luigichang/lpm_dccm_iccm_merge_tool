/*
 ============================================================================
 Name        : lpm_merge_tool.c
 Author      : Luigi Chang
 Version     : v1.0
 Copyright   : 2017 Luigi Chang All Rights Reserved
 Description : loader iccm/dccm convert to one binary file in C, Ansi-style
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define COMPANY_NAME			"LITE-ON STORAGE/MicroSSD Team"
#define VERSION					"v1.0"
#define LOADER_ICCM_FILE_NAME	"PS5008_LPM_LOADER_ICCM0_input.bin"
#define LOADER_DCCM_FILE_NAME	"PS5008_LPM_LOADER_DCCM1.bin"
#define LOADER_OUTPUT_FILE_NAME	"PS5008_DATA_PAYLOAD.bin"
#define MAX_LEN					80

typedef unsigned int	UDWORD;

int main()
{
	char in_iccm_file_name[MAX_LEN], in_dccm_file_name[MAX_LEN], out_file_name[MAX_LEN] ;
	FILE *fp_in_loader_iccm, *fp_in_loader_dccm, *fp_out;

	UDWORD data_loader_iccm[0x8000 / 4] ;
	UDWORD data_loader_dccm[0x4000 / 4] ;

	printf("%s - PS5008 LPM Loader ICCM/DCCM merge tool %s\n\n", COMPANY_NAME, VERSION);
	printf("[noted: press enter to use default file name]\n");

	printf("Please input loader - iccm file name (default is %s): ", LOADER_ICCM_FILE_NAME);
	fgets(in_iccm_file_name, MAX_LEN, stdin);

	if((strlen(in_iccm_file_name) == 1) && (in_iccm_file_name[0] == 0x0a)) strcpy(in_iccm_file_name, LOADER_ICCM_FILE_NAME) ;
	else in_iccm_file_name[strlen(in_iccm_file_name) - 1] = 0x00 ;

	printf("Please input loader - dccm file name (default is %s): ", LOADER_DCCM_FILE_NAME);
	fgets(in_dccm_file_name, MAX_LEN, stdin);

	if((strlen(in_dccm_file_name) == 1) && (in_dccm_file_name[0] == 0x0a)) strcpy(in_dccm_file_name, LOADER_DCCM_FILE_NAME) ;
	else in_dccm_file_name[strlen(in_dccm_file_name) - 1] = 0x00 ;

	printf("Please input loader - output file name (default is %s): ", LOADER_OUTPUT_FILE_NAME);
	fgets(out_file_name, MAX_LEN, stdin);

	if((strlen(out_file_name) == 1) && (out_file_name[0] == 0x0a)) strcpy(out_file_name, LOADER_OUTPUT_FILE_NAME) ;
	else out_file_name[strlen(out_file_name) - 1] = 0x00 ;

	printf("\n") ;
	printf("The input iccm file name: [%s]\n", in_iccm_file_name) ;
    printf("The input dccm file name: [%s]\n", in_dccm_file_name) ;
    printf("The output file name: [%s]\n", out_file_name) ;

	fp_in_loader_iccm = fopen(in_iccm_file_name, "rb");
	if(fp_in_loader_iccm != NULL) fread(data_loader_iccm, 4, (0x8000 / 4), fp_in_loader_iccm);
	else
	{
		printf("\n");
		printf("Cannot find the input iccm-file: %s\n", in_iccm_file_name);
		goto EXIT2;
	}

    fp_in_loader_dccm = fopen(in_dccm_file_name, "rb");
    if(fp_in_loader_dccm) fread(data_loader_dccm, 4, (0x4000 / 4), fp_in_loader_dccm);
    else
    {
    	printf("\n");
		printf("Cannot find the input dccm-file: %s\n", in_dccm_file_name);
		goto EXIT1;
    }

	fp_out = fopen(out_file_name, "wb+") ;
	if(fp_out)
	{
		fwrite(&data_loader_iccm, 4, (0x8000 / 4), fp_out);
		fwrite(&data_loader_dccm, 4, (0x4000 / 4), fp_out);
	}
	else
	{
		printf("\n");
		printf("Cannot find the input output-file: %s\n", out_file_name);
		goto EXIT0;
	}

	printf("\n");
	printf("Loader ICCM/DCCM files merge success!\n");

    fclose(fp_out);
EXIT0:
    fclose(fp_in_loader_dccm);
EXIT1:
    fclose(fp_in_loader_iccm);
EXIT2:

    return 0;
}
